let utilidades = require('./utilidades.js')
var assert = require('assert');
describe('Division Prueba uno', () => {
  it('should test if 25/5 = 5', () => {
    assert.equal(5, utilidades.division(25, 5));
  });
});

describe('Division Prueba Dos', () => {
  it('should test if 15/5 = 3', () => {
    assert.equal(3, utilidades.division(15, 5));
  });
});

describe('Caracter prueba uno', () => {
  it('should test if wa1iner = 1', () => {
    assert.equal(1, utilidades.extraerNumero('wa1iner'));
  });
});

describe('Caracter prueba dos', () => {
  it('should test if asf3e2 = 3', () => {
    assert.equal(3, utilidades.extraerNumero('asf3e2'));
  });
});