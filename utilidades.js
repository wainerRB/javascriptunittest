const utilidades =  {
  division(divisor, dividendo){
    if(!divisor || !dividendo){
      return -1;
    }
    return divisor/dividendo;
  },
  extraerNumero(text){
    if(!text){
      return -1;
    }
    for (let index = 0; index < text.length; index++) {
      if((!isNaN(parseInt(text[index])))){
        return text[index]
      } 
    }
    return -1;
  }
};

module.exports = utilidades;